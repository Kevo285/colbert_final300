﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCheck : MonoBehaviour
{
    public GameObject Hitbox;
    public Animator anim;
    public int enemycount;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    if (anim.GetBool("Attack1"))
        {

            Hitbox.SetActive(true);

        }
        else
        {
            Hitbox.SetActive(false);
        }



    }


    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Enemy")
        {
            Destroy(col.gameObject);
        }
    }



}
