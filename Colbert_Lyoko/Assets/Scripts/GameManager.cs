﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public int currentTowers;
    public Text objectiveText;
    
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    
    public AudioSource exitAudio;

    public Camera AliveCam, DeadCam;


    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    public GameObject myHealth;
    // Start is called before the first frame update
    void Start()
    {
        DeadCam.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(currentTowers == 2)
        {
    objectiveText.text = "Nice. Alright, time to devirualize you.";
            EndLevel(true, exitAudio);
        }
        else
        {
                if(GameObject.Find("HealthBar").GetComponent<Health>().currentHealth <= 0)
                {
                    BadEndLevel(true, exitAudio);
                objectiveText.text = "Your health reached zero, and you were lost to the world's data forever.";
                    Destroy(player.gameObject);
                DeadCam.gameObject.SetActive(true);
               
                 }
        }
        
    }

public void AddGold(int goldToAdd)
    {

        currentTowers += goldToAdd;
        

    }

    void EndLevel(bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            

            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }


    void BadEndLevel(bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;


        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }


}



