﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class combohits : MonoBehaviour
{
    public Animator anim;
    public int numClicks = 0;
    float lastclickTime = 0;
    float maxComboDelay = 0.7f;
    public GameObject Hitbox;
    public AudioSource audiosword;
    public AudioClip[] audioArray;
    public AudioClip[] finisher;
    public AudioClip[] blocker;
    public AudioClip[] charger;
    public AudioClip[] damager;
    public AudioClip[] Special;
    public AudioClip[] Flight;
    public AudioClip[] HitEffects;
    public AudioClip[] Dead;
    public AudioClip[] HealSounds;
    public AudioClip[] FlySound;


    void Awake()
    {
 audiosword = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        Hitbox.GetComponent<BoxCollider>();


    }

    // Update is called once per frame
    void Update()
    {

        attacking();
        Heal();
        Block();
        SpecialMove();
        ChargeEnergy();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();

        }





    }

    public void return1()
    {
        if(numClicks >= 2)
        {
            anim.SetBool("Attack2", true);
            Hitbox.SetActive(true);
        }
        else
        {
            anim.SetBool("Attack1", false);
            numClicks = 0;
            Hitbox.SetActive(false);
        }

    }

    public void return2()
    {
        if (numClicks >= 2)
        {
            anim.SetBool("Attack3", true);
        }
        else
        {
            anim.SetBool("Attack2", false);
            numClicks = 0;
        }

    }

    public void return3()
    {
       
        anim.SetBool("Attack1", false);
        anim.SetBool("Attack2", false);
        anim.SetBool("Attack3", false);
        numClicks = 0;
        

    }
    //Sound Arrays (to mix it up a bit)
    public void attacksounds()
    {

        audiosword.clip = audioArray[Random.Range(0, 4)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void finishers()
    {
        audiosword.clip = finisher[Random.Range(0, 2)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void blocking()
    {
        audiosword.clip = blocker[Random.Range(0, 2)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void charging()
    {
        audiosword.clip = charger[Random.Range(0, 2)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void damagesounds()
    {
        audiosword.clip = damager[Random.Range(0, 4)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void Specialsounds()
    {
        audiosword.clip = Special[Random.Range(0, 4)];
        audiosword.PlayOneShot(audiosword.clip);
  
    }
    public void Flightsounds()
    {
        audiosword.clip = Flight[Random.Range(0, audioArray.Length)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void Swordhitsounds()
    {
        audiosword.clip = HitEffects[Random.Range(0, audioArray.Length)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void deadsounds()
    {
        audiosword.clip = Dead[Random.Range(0, 3)];
        audiosword.PlayOneShot(audiosword.clip);
    }
    public void healing()
    {
        
        audiosword.clip = HealSounds[Random.Range(0, 2)];
        audiosword.PlayOneShot(audiosword.clip);
        GameObject.Find("HealthBar").GetComponent<Health>().HealPlayer(35);
    }
    // Attack/Defend Commands
   
    public void Heal()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {

            anim.SetBool("isHealing", true);
           
        }
        else
        {
            anim.SetBool("isHealing", false);
        }
    }
    public void Block()
    {
        if (Input.GetKey(KeyCode.E))
        {

            anim.SetBool("isBlocking", true);
            
        }
        else
        {
            anim.SetBool("isBlocking", false);
        }
    }
    public void SpecialMove()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            int moveDMG = 50;

            anim.SetBool("isSuper", true);
        }
        else
        {
            anim.SetBool("isSuper", false);
        }
    }
    public void ChargeEnergy()
    {
        if (Input.GetKey(KeyCode.X))
        {

            anim.SetBool("isCharging", true);
        }
        else
        {
            anim.SetBool("isCharging", false);
        }
    }
    public void attacking()
    {
        if (Time.time - lastclickTime > maxComboDelay)
        {

            numClicks = 0;
        }

        if (Input.GetMouseButtonDown(0))
        {
            lastclickTime = Time.time;
            numClicks++;
            if (numClicks == 1)
            {
                anim.SetBool("Attack1", true);
            }
            numClicks = Mathf.Clamp(numClicks, 0, 3);
        }
    }



    public void FlyingSound()
    {
        if (anim.GetBool("Fly"))
        {
            audiosword.clip = FlySound[Random.Range(0, audioArray.Length - 1)];
            audiosword.PlayOneShot(audiosword.clip);
            
        }
     
    }
}
