﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour
{
    public int speed = 40;
    public GameObject thefab;
    // Start is called before the first frame update
    void Start()
    {
        thefab = Resources.Load("Projectile") as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            GameObject Projectile = Instantiate(thefab) as GameObject;
            Projectile.transform.position = transform.position + Camera.main.transform.forward;
            Rigidbody rb = Projectile.GetComponent<Rigidbody>();
            rb.velocity = Camera.main.transform.forward * speed;
        }
    }
}
