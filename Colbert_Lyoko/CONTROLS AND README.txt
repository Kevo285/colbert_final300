Q Use Potion
W Move Up
A Move Down
S Move Left
D Move Right
Z Special Move
X Charge Dark Energy
E Block/Guard
Shift Sprint/Fly Faster
Mouse Click: Attack/Combo Finisher
UPDATE POST PROCESSING BECAUSE OF A BUG WITH 9.2. 


update the Post Processing instead of deleting it. To update just go to Window > Package Manager and switch the view from In Project to All. Select Postprocessing in the list. Then update it the latest version.

I've provided a build just in case.